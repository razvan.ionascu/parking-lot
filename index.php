<?php
/**
 * Created by Razvan <razvan@razvanionascu.com>.
 */

use ParkingLot\Process\Processor;

require_once 'includes.php';

spl_autoload_register(function ($class_name) {

    $class_name = substr_replace($class_name,'src', 0, 10 );
    $class_name = str_replace('\\', '/', $class_name);
    include $class_name . '.php';
});

$processor = Processor::get();
$processor->process();