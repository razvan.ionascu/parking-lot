<?php

namespace ParkingLot\FileHandler\FileWriter;

use ParkingLot\Error\FileHandler\FileWriterException;
use ParkingLot\FileHandler\AbstractFileHandler;

/**
 * Class FileWriter
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class FileWriter extends AbstractFileHandler
{
    /**
     * @throws FileWriterException
     */
    public function writeFile()
    {
        if (!is_dir($this->folder)) {
            mkdir($this->folder, 0777, true);
        }

        $this->validateFolder();

        try {
            if (false == file_put_contents($this->path, $this->fileContent)) {
                throw new FileWriterException("Could not create file: {$this->path}");
            }
        } catch (\Exception $e) {
            throw new FileWriterException("Could not create file: {$this->path}. Error: {$e->getMessage()}");
        }

        $this->validateFile();
    }


    /**
     * @return string
     */
    public function getFileContent()
    {
        return $this->fileContent;
    }

    /**
     * @param $fileContent
     */
    public function setFileContent($fileContent)
    {
        $this->fileContent = $fileContent;
    }
}