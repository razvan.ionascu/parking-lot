<?php

namespace ParkingLot\FileHandler;

use ParkingLot\Error\FileHandler\InvalidPathException;

/**
 * Class AbstractFileHandler
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
abstract class AbstractFileHandler
{
    /**
     * @var string
     */
    protected $folder;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $fileContent;

    /**
     * AbstractFileHandler constructor.
     * @param $folder
     * @param $fileName
     */
    public function __construct($folder, $fileName)
    {
        if (is_string($folder)) {
            $this->folder = $folder;
        }

        if (is_string($fileName)) {
            $this->fileName = $fileName;
        }

        $this->validateFolder();
        $this->path = $this->folder . '/' . $this->fileName;

    }

    /**
     * @return bool
     * @throws InvalidPathException
     */
    protected function validateFolder()
    {
        if (!is_dir($this->folder)) {
            throw new InvalidPathException("Invalid folder path provided: {$this->folder}");
        }

        return true;
    }

    /**
     * @return bool
     * @throws InvalidPathException
     */
    protected function validateFile()
    {
        if (!file_exists($this->path)) {
            throw new InvalidPathException("Invalid file path provided: {$this->path}");
        }

        if (!is_readable($this->path)) {
            throw new InvalidPathException("The given file is not readable: {$this->path}");
        }

        return true;
    }

    /**
     * @return string
     */
    abstract public function getFileContent();
}