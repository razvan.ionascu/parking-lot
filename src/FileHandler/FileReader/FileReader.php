<?php

namespace ParkingLot\FileHandler\FileReader;

use ParkingLot\Error\FileHandler\FileReaderException;
use ParkingLot\FileHandler\AbstractFileHandler;

/**
 * Class FileReader
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class FileReader extends AbstractFileHandler
{

    /**
     * @return string
     * @throws FileReaderException
     */
    public function getFileContent()
    {
        if (!$this->fileContent) {
            $this->validateFile();

            try {
                $this->fileContent = file_get_contents($this->path);
                if (false === $this->fileContent) {
                    throw new FileReaderException("Could not read from file: {$this->path}");
                }
            } catch (\Exception $e) {
                throw new FileReaderException("Could not read from file: {$this->path}. Error {$e->getMessage()}");
            }

        }

        return $this->fileContent;

    }

}