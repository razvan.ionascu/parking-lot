<?php

namespace ParkingLot\FileInterpreter;

use ParkingLot\Entity\ParkingLot\ParkingLot;

/**
 * Class FileInterpreter
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class FileInterpreter
{
    const INLINE_SEPARATOR       = ':';
    const PARKING_SPOTS_CODE     = 'N';
    const POLLUTION_BUFFER_CODE  = 'B';
    const ENTRANCE_COUNT_CODE    = 'X';
    const ENTRANCE_SEQUENCE_CODE = 'I';
    const PARKING_SEQUNCE_CODE   = 'P';
    const EXIT_SEQUENCE_CODE     = 'E';

    /**
     * @var string
     */
    private $fileContentString;

    /**
     * @var array
     */
    private $fileContentArray;

    /**
     * @var ParkingLot
     */
    private $parkingLot;

    /**
     * FileInterpreter constructor.
     * @param string $content
     */
    public function __construct($content)
    {
        if (is_string($content)) {
            $this->fileContentString = $content;
        }
    }

    /**
     * @return ParkingLot
     */
    public function process()
    {
        foreach ($this->getContentArray() as $inputLine) {
            $explodedLine = explode(self::INLINE_SEPARATOR, $inputLine);
            $lineCode     = reset($explodedLine);
            $lineValue    = end($explodedLine);

            $this->processLine($lineCode, $lineValue);
        }

        $this->getParkingLot()->validate();
        $this->getParkingLot()->determineMaxCarsRunningCount();

        return $this->getParkingLot();
    }

    /**
     * @param string $lineCode
     * @param string $lineValue
     */
    private function processLine($lineCode, $lineValue)
    {
        $parkingLot = $this->getParkingLot();

        switch ($lineCode) {
            case self::PARKING_SPOTS_CODE :
                $parkingLot->setParkingSpotCount((int)$lineValue);
                break;
            case self::POLLUTION_BUFFER_CODE:
                $parkingLot->setPollutionPercent((int)$lineValue);
                break;
            case self::ENTRANCE_COUNT_CODE:
                $parkingLot->setEntranceCount((int)$lineValue);
                break;
            case self::ENTRANCE_SEQUENCE_CODE:
                $parkingLot->addEntrance($lineValue);
                break;
            case self::PARKING_SEQUNCE_CODE:
                $parkingLot->setParkingSequence($lineValue);
                break;
            case self::EXIT_SEQUENCE_CODE :
                $parkingLot->setExitSequence($lineValue);
                break;
        }
    }

    /**
     * @return array
     */
    private function getContentArray()
    {
        if (!$this->fileContentArray) {
            $this->fileContentArray = preg_split("/(\r\n|\n|\r)/", $this->fileContentString);
        }

        return $this->fileContentArray;
    }

    /**
     * @return ParkingLot
     */
    private function getParkingLot()
    {
        if (!$this->parkingLot instanceof ParkingLot) {
            $this->parkingLot = new ParkingLot();
        }

        return $this->parkingLot;
    }


}