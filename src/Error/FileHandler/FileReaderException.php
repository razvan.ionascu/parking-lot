<?php

namespace ParkingLot\Error\FileHandler;

/**
 * Class FileReaderException
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class FileReaderException extends \Exception
{
    //
}