<?php

namespace ParkingLot\Error\FileHandler;

/**
 * Class InvalidPathException
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class InvalidPathException extends \Exception
{
    //
}