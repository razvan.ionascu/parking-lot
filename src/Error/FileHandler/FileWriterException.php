<?php

namespace ParkingLot\Error\FileHandler;

/**
 * Class FileWriterException
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class FileWriterException extends \Exception
{
    //
}