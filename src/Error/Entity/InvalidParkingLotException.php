<?php

namespace ParkingLot\Error\Entity;

/**
 * Class InvalidParkingLotError
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class InvalidParkingLotException extends \Exception
{
    //
}