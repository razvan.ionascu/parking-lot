<?php

namespace ParkingLot\Entity\ParkingLot;


use ParkingLot\Entity\Sequence\InputSequenceInterface;
use ParkingLot\Error\Entity\InvalidParkingLotException;

interface ParkingLotInterface
{
    /**
     * @return int
     */
    public function getParkingSpotCount();

    /**
     * @param int $parkingSpotCount
     * @return ParkingLotInterface
     */
    public function setParkingSpotCount($parkingSpotCount);

    /**
     * @return int
     */
    public function getPollutionPercent();

    /**
     * @param int $pollutionPercent
     * @return ParkingLotInterface
     */
    public function setPollutionPercent($pollutionPercent);

    /**
     * @return int
     */
    public function getMaxCarsRunningCount();

    /**
     * @param int $maxCarsRunningCount
     * @return ParkingLotInterface
     */
    public function setMaxCarsRunningCount($maxCarsRunningCount);

    /**
     * @return int
     */
    public function getEntranceCount();

    /**
     * @param int $entranceCount
     * @return ParkingLotInterface
     */
    public function setEntranceCount($entranceCount);

    /**
     * @return InputSequenceInterface[]
     */
    public function getEntrances();

    /**
     * @param $index
     * @return null|InputSequenceInterface
     */
    public function getEntrance($index);

    /**
     * @param InputSequenceInterface[] $entrances
     * @return ParkingLot
     */
    public function setEntrances($entrances);

    /**
     * @return InputSequenceInterface
     */
    public function getParkingSequence();

    /**
     * @param string $parkingSequence
     * @return ParkingLot
     */
    public function setParkingSequence($parkingSequence);

    /**
     * @return InputSequenceInterface
     */
    public function getExitSequence();

    /**
     * @param InputSequenceInterface $exitSequence
     * @return ParkingLot
     */
    public function setExitSequence($exitSequence);

    /**
     * @throws InvalidParkingLotException
     */
    public function validate();
}