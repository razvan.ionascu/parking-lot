<?php

namespace ParkingLot\Entity\ParkingLot;

use ParkingLot\Entity\Entrance\EntranceInputSequence;
use ParkingLot\Entity\Leaving\LeavingSequence;
use ParkingLot\Entity\Parking\ParkingSequence;
use ParkingLot\Error\Entity\InvalidParkingLotException;

/**
 * Class ParkingLot
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class ParkingLot
{
    /**
     * @var int
     */
    private $parkingSpotCount;

    /**
     * @var int
     */
    private $pollutionPercent;

    /**
     * @var int
     */
    private $maxCarsRunningCount;

    /**
     * @var int
     */
    private $entranceCount;

    /**
     * @var EntranceInputSequence[]
     */
    private $entrances;

    /**
     * @var ParkingSequence
     */
    private $parkingSequence;

    /**
     * @var EntranceInputSequence
     */
    private $exitSequence;

    /**
     * ParkingLot constructor.
     * @param $parkingSpotCount
     */
    public function __construct($parkingSpotCount = null)
    {
        $this->setParkingSpotCount($parkingSpotCount);
    }

    /**
     * @return int
     */
    public function getParkingSpotCount()
    {
        return $this->parkingSpotCount;
    }

    /**
     * @param int $parkingSpotCount
     * @return ParkingLot
     */
    public function setParkingSpotCount($parkingSpotCount)
    {
        $this->parkingSpotCount = $parkingSpotCount;

        return $this;
    }

    /**
     * @return int
     */
    public function getPollutionPercent()
    {
        return $this->pollutionPercent;
    }

    /**
     * @param int $pollutionPercent
     * @return ParkingLot
     */
    public function setPollutionPercent($pollutionPercent)
    {
        $this->pollutionPercent = $pollutionPercent;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxCarsRunningCount()
    {
        return $this->maxCarsRunningCount;
    }

    /**
     * @param int $maxCarsRunningCount
     * @return ParkingLot
     */
    public function setMaxCarsRunningCount($maxCarsRunningCount)
    {
        $this->maxCarsRunningCount = $maxCarsRunningCount;

        return $this;
    }

    /**
     *
     */
    public function determineMaxCarsRunningCount()
    {
        if (is_numeric($this->pollutionPercent)) {
            $this->maxCarsRunningCount = (int)(($this->parkingSpotCount * $this->pollutionPercent) / 100);
        } else {
            $this->maxCarsRunningCount = $this->parkingSpotCount;
        }
    }

    /**
     * @return int
     */
    public function getEntranceCount()
    {
        return $this->entranceCount;
    }

    /**
     * @param int $entranceCount
     * @return ParkingLot
     */
    public function setEntranceCount($entranceCount)
    {
        $this->entranceCount = $entranceCount;

        return $this;
    }

    /**
     * @return EntranceInputSequence[]
     */
    public function getEntrances()
    {
        return $this->entrances;
    }

    /**
     * @param $index
     * @return null|EntranceInputSequence
     */
    public function getEntrance($index)
    {
        return isset($this->entrances[$index]) ? $this->entrances[$index] : null;
    }

    /**
     * @param EntranceInputSequence[] $entrances
     * @return ParkingLot
     */
    public function setEntrances($entrances)
    {
        $this->entrances = $entrances;

        return $this;
    }

    /**
     * @param string $entranceSequenceString
     * @return $this
     */
    public function addEntrance($entranceSequenceString)
    {
        $this->entrances[] = new EntranceInputSequence(str_split($entranceSequenceString));

        return $this;
    }

    /**
     * @return ParkingSequence
     */
    public function getParkingSequence()
    {
        return $this->parkingSequence;
    }

    /**
     * @param string $parkingSequence
     * @return ParkingLot
     */
    public function setParkingSequence($parkingSequence)
    {
        $this->parkingSequence = new ParkingSequence(str_split($parkingSequence));

        return $this;
    }

    /**
     * @return EntranceInputSequence
     */
    public function getExitSequence()
    {
        return $this->exitSequence;
    }

    /**
     * @param string $exitSequence
     * @return ParkingLot
     */
    public function setExitSequence($exitSequence)
    {
        $this->exitSequence = new LeavingSequence(str_split($exitSequence));

        return $this;
    }

    /**
     * @throws InvalidParkingLotException
     */
    public function validate()
    {
        if (!is_numeric($this->parkingSpotCount)) {
            throw new InvalidParkingLotException('Invalid parking lot. Unknown number of parking spots');
        }
    }
}