<?php

namespace ParkingLot\Entity\Leaving;


use ParkingLot\Entity\Sequence\AbstractInputSequence;

/**
 * Class LeavingSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class LeavingSequence
    extends AbstractInputSequence
{
    //
}