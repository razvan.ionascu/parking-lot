<?php

namespace ParkingLot\Entity\Entrance;

use ParkingLot\Entity\Sequence\AbstractInputSequence;

/**
 * Class Entrance
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class EntranceInputSequence
    extends AbstractInputSequence
{
    //
}