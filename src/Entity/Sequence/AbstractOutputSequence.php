<?php

namespace ParkingLot\Entity\Sequence;

/**
 * Class AbstractOutputSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class AbstractOutputSequence
    extends AbstractInputSequence
    implements OutputSequenceInterface
{
    /**
     * @param int $value
     * @return AbstractOutputSequence
     */
    public function addToSequence($value)
    {
        $this->sequence[] = $value;

        return $this;
    }
}