<?php

namespace ParkingLot\Entity\Sequence;

/**
 * Class EntranceInterface
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
interface InputSequenceInterface
{

    /**
     * @param array $sequence
     * @return InputSequenceInterface
     */
    public function setSequence($sequence);

    /**
     * @return array
     */
    public function getSequence();

    /**
     * @param int $sequenceNumber
     * @return int
     */
    public function getSpecificSequence($sequenceNumber);
}