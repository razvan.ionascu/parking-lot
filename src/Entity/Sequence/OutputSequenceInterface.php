<?php

namespace ParkingLot\Entity\Sequence;

/**
 * Interface OutputSequenceInterface
 * @package ParkingLot\Entity\Sequence
 */
interface OutputSequenceInterface extends InputSequenceInterface
{
    /**
     * @param array $sequence
     * @return mixed
     */
    public function setSequence($sequence);

    /**
     * @param int $value
     * @return mixed
     */
    public function addToSequence($value);
}