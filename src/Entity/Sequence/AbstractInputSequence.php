<?php

namespace ParkingLot\Entity\Sequence;

/**
 * Class AbstractInputSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
abstract class AbstractInputSequence implements InputSequenceInterface
{
    /**
     * @var array
     */
    protected $sequence;

    /**
     * AbstractSequence constructor.
     * @param $sequence
     */
    public function __construct($sequence = null)
    {
        $this->setSequence($sequence);
    }

    /**
     * @param array $sequence
     * @return AbstractInputSequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * @return array
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param int $sequenceNumber
     * @return int
     */
    public function getSpecificSequence($sequenceNumber)
    {
//        used isset to avoid warnings
        return isset($this->sequence[$sequenceNumber]) ? $this->sequence[$sequenceNumber] : null;
    }

    /**
     * @param int $sequenceNumber
     * @param int $value
     */
    public function setSpecificSequence($sequenceNumber, $value)
    {
        $this->sequence[$sequenceNumber] = $value;
    }
}