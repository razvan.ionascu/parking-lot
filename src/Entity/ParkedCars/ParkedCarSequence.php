<?php

namespace ParkingLot\Entity\ParkedCars;

use ParkingLot\Entity\Sequence\AbstractOutputSequence;

/**
 * Class ParkedCarSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class ParkedCarSequence
    extends AbstractOutputSequence
{
    //
}