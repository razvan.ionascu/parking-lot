<?php

namespace ParkingLot\Entity\Report;

use ParkingLot\Entity\AllCars\AllCarsSequence;
use ParkingLot\Entity\ParkedCars\ParkedCarSequence;
use ParkingLot\Entity\RunningCars\RunningCarsSequence;

/**
 * Class AbstractOutputReport
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class AbstractOutputReport
{
    const ALL_CARS_CODE    = 'T';
    const PARKED_CARS_CODE = 'P';
    const RUNNIG_CARS_CODE = 'R';

    /**
     * @var AllCarsSequence
     */
    private $allCarsSequence;

    /**
     * @var ParkedCarSequence
     */
    private $parkedCarsSequence;

    /**
     * @var RunningCarsSequence
     */
    private $runningCarsSequnce;

    /**
     * @param int $value
     * @return $this
     */
    public function addToAllCarsSequence($value)
    {
        if (!$this->allCarsSequence instanceof AllCarsSequence) {
            $this->allCarsSequence = new AllCarsSequence();
        }

        $this->allCarsSequence->addToSequence($value);

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function addToParkedCarsSequence($value)
    {
        if (!$this->parkedCarsSequence instanceof ParkedCarSequence) {
            $this->parkedCarsSequence = new ParkedCarSequence();
        }

        $this->parkedCarsSequence->addToSequence($value);

        return $this;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function addToRunningCarsSequence($value)
    {
        if (!$this->runningCarsSequnce instanceof RunningCarsSequence) {
            $this->runningCarsSequnce = new RunningCarsSequence();
        }

        $this->runningCarsSequnce->addToSequence($value);

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $string = self::ALL_CARS_CODE . ':' . implode('', $this->allCarsSequence->getSequence()) . "\n";
        $string .= self::PARKED_CARS_CODE . ':' . implode('', $this->parkedCarsSequence->getSequence()) . "\n";
        $string .= self::RUNNIG_CARS_CODE . ':' . implode('', $this->runningCarsSequnce->getSequence()) . "\n";

        return $string;
    }
}