<?php

namespace ParkingLot\Entity\Parking;

use ParkingLot\Entity\Sequence\AbstractInputSequence;

/**
 * Class ParkingSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class ParkingSequence
    extends AbstractInputSequence
{
    //
}