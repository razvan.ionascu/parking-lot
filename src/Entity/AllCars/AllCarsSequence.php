<?php

namespace ParkingLot\Entity\AllCars;

use ParkingLot\Entity\Sequence\AbstractOutputSequence;

/**
 * Class AllCarsSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class AllCarsSequence
    extends AbstractOutputSequence
{
    //
}