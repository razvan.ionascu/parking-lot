<?php

namespace ParkingLot\Entity\RunningCars;

use ParkingLot\Entity\Sequence\AbstractOutputSequence;

/**
 * Class RunningCarsSequence
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class RunningCarsSequence
    extends AbstractOutputSequence
{
    //
}