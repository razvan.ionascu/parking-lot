<?php

namespace ParkingLot\Process;

use ParkingLot\Entity\ParkingLot\ParkingLot;
use ParkingLot\Entity\Report\OutputReport;
use ParkingLot\Error\Entity\InvalidParkingLotException;
use ParkingLot\Error\FileHandler\FileReaderException;
use ParkingLot\Error\FileHandler\FileWriterException;
use ParkingLot\Error\FileHandler\InvalidPathException;
use ParkingLot\FileHandler\FileReader\FileReader;
use ParkingLot\FileHandler\FileWriter\FileWriter;
use ParkingLot\FileInterpreter\FileInterpreter;

/**
 * Class Processor
 * Created by Razvan Ionascu <razvan@razvanionascu.com>
 */
class Processor
{
    const READING_FOLDER = __ROOTDIR__ . '/tmp';

    const WRITING_FOLDER = __ROOTDIR__ . '/tmp';

    const INPUT_FILE_NAME = 'input.txt';

    const OUTPUT_FILE_NAME = 'output.txt';

    /**
     * @var ParkingLot
     */
    private $parkingLot = null;

    /**
     * @var OutputReport
     */
    private $report = null;

    /**
     * @var int
     */
    private $carsCount = 0;

    /**
     * @var int
     */
    private $parkedCarsCount = 0;

    /**
     * @var int
     */
    private $runningCarsCount = 0;

    /**
     * @var Processor
     */
    public static $instance;

    /**
     * Processor constructor.
     */
    public function __construct() {
        self::$instance = $this;
    }

    /**
     * @return Processor
     */
    public static function get() {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }


    public function process()
    {

        try {

            $parkingLot          = $this->getParkingLot();
            $sequenceCount       = count($parkingLot->getExitSequence()->getSequence());
            $parkingLotEntrances = $parkingLot->getEntrances();

            for ($sequenceIndex = 0; $sequenceIndex < $sequenceCount; $sequenceIndex ++) {

                $carExits = $parkingLot->getExitSequence()->getSpecificSequence($sequenceIndex);
                if ($carExits) {
                    $this->carsCount--;
                }


                foreach ($parkingLotEntrances as $entrance) {

                    $carEnters = $entrance->getSpecificSequence($sequenceIndex);

                    if ($carEnters) {
                        // the car might exit without paying and the action would be marked as entry$carEnters
                        if (2 === (int)$parkingLot->getParkingSequence()->getSpecificSequence($sequenceIndex + 1)) {

                            $parkingLot->getParkingSequence()->setSpecificSequence($sequenceIndex + 1, -1);
                            $this->carsCount--;
                        } else {
                            //check the pollution
                            if ($this->runningCarsCount < $parkingLot->getMaxCarsRunningCount()) {
                                $this->carsCount++;
                            } else {
                                echo 'Pollution problem. Cars can not entry unless other cars leave the parkin lot';
                            }

                        }


                    }
                }

                $carParks = $parkingLot->getParkingSequence()->getSpecificSequence($sequenceIndex);
                    $this->parkedCarsCount += $carParks;

                $this->runningCarsCount = $this->carsCount - $this->parkedCarsCount;

                $this->getOutputReport()->addToAllCarsSequence($this->carsCount)
                    ->addToParkedCarsSequence($this->parkedCarsCount)->
                    addToRunningCarsSequence($this->runningCarsCount);

            }

            $this->writeReport();

            echo 'Successfully generated the report';

        } catch (FileReaderException $e) {
            echo $e;
            echo '<br/>';
        } catch (FileWriterException $e) {
            echo $e;
            echo '<br/>';
        } catch (InvalidPathException $e) {
            echo $e;
        } catch (InvalidParkingLotException $e) {
            echo $e;
        } catch (\Exception $e) {
            echo $e;
        }
    }

    /**
     * @return ParkingLot
     */
    private function getParkingLot()
    {
        if (!$this->parkingLot instanceof ParkingLot) {

            $fileReader       = new FileReader(self::READING_FOLDER, self::INPUT_FILE_NAME);
            $fileInterpreter  = new FileInterpreter($fileReader->getFileContent());
            $this->parkingLot = $fileInterpreter->process();
        }

        return $this->parkingLot;
    }

    /**
     * @return OutputReport
     */
    private function getOutputReport()
    {
        if (!$this->report instanceof OutputReport) {
            $this->report = new OutputReport();
        }

        return $this->report;
    }

    private function writeReport()
    {
        $fileWriter = new FileWriter(self::WRITING_FOLDER, self:: OUTPUT_FILE_NAME);
        $fileWriter->setFileContent($this->getOutputReport()->__toString());
        $fileWriter->writeFile();
    }

    /**
     * @return int
     */
    public function getRunningCarsCount()
    {
        return $this->runningCarsCount;
    }

    /**
     * @return int
     */
    public function getCarsCount()
    {
        return $this->carsCount;
    }
}